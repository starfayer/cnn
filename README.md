# Сверточная нейронная сеть для распознавания аудиообразов

Данная работа посвящена задаче распознавания аудиообразов.  
Для решения данной задачи была создана, обучена и протестирована сверточная нейронная сеть глубокого обучения, а также проведено исследование точности работы нейросети в зависимости от типа подаваемых данных на вход сети.  

## Постановка задачи
Пусть имеется определённое количество аудиофайлов и несколько классов, к которым теоретически может относиться каждый файл. Класс можно интерпретировать как тип, содержание аудиозаписи (отбойный молоток, шум вентилятора, уличная музыка).  
Задача состоит в том, чтобы определить, к какому классу относится каждый из файлов.

## Постановка решения
На сегодняшний день известен не один алгоритм классификации образов, однако каждый из них обладает своими недостатками.
| Метод | Преимущества | Недостатки
| - | - | - |
| Дискриминантный анализ| Простота реализации. Низкие вычислительные затраты | Низкое качество классификации 
| Скрытая Марковская модель| Универсальность| Высокие вычислительные затраты |
| Динамическое программирование | Хорошая работа с маленькими словарями | Плохая работа с большими словарями |
| Нейронные сети| Хорошая работа с зашумленными данными | Необходимость в больших обучающих выборках |

Нейронная сеть может похвастаться скоростью решения задачи и точности, что и становится ключевым фактором её выбора.  

Для решения задачи мы можем получить заведомо верные примеры данных (датасет), который может будем использовать для обучения нейронной сети. Таким образом нейронная сеть получает возможность использования "обучения с учителем" на заранее известных данных.

### Формат входных данных
Объемы входных данных будут зависеть от используемого алгоритма обработки аудиоданных.  
Для работы с аудиофайлами с помощью нейронной сети необходимо воспользоваться физической природой аудиообразов. Поскольку аудиоданные по своей сути являются сигналом, они могут быть представлены в виде дискретном виде. С помощью преобразования Фурье любой дискретный сигнал можно представить в виде спектра - характеристики сигнала (амплитуды или частоты) в определённые моменты времени. Спектр каждого аудиосигнала будет отличен, тем самым мы сможем разделять и хранить каждый файл в небольшом объеме данных.  

Спектр сигнала можно представить в виде спектрограммы - "водопадной" диаграммы, на которой по горизонтальной оси отложено время, по вертикальной - частота сигнала, а по третьему измерению (цвету изображения) в зависимости от яркости можно определить амплитуду. 

На сегодняшний день помимо хранения и обработки аудиоданных в спектральном виде используется также представление в виде мел-кепстральных коэффициентов. "Мел" является некой величиной восприятия звука человеческими органами слуха, то есть нормированная величина амплитуды или частоты. "Кепстр" обозначает несколько операций, которые необходимо провести над "мел"-величинами, в том числе тоже самое преобразование Фурье (которое используется для составления спектра, так как сигнал можно представить в виде ряда). Вместе с преобразованием Фурье применяется операция логарифмирования, в результате чего форма входных параметров уменьшается.  

Исследование в данной работе направлено на изучение точности распознавания нейросети в зависимости алгоритма обработки обучающих и тестовых данных.

## Архитектура нейронной сети 

<pre style="white-space:pre;overflow-x:auto;line-height:normal;font-family:Menlo,'DejaVu Sans Mono',consolas,'Courier New',monospace">┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┳━━━━━━━━━━━━━━━━━━━━━━━━┳━━━━━━━━━━━━━━━┓
┃<span style="font-weight: bold"> Layer (type)                    </span>┃<span style="font-weight: bold"> Output Shape           </span>┃<span style="font-weight: bold">       Param # </span>┃
┡━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━╇━━━━━━━━━━━━━━━━━━━━━━━━╇━━━━━━━━━━━━━━━┩
│ Conv_1 (<span style="color: #0087ff; text-decoration-color: #0087ff">Conv2D</span>)                 │ (<span style="color: #00d7ff; text-decoration-color: #00d7ff">None</span>, <span style="color: #00af00; text-decoration-color: #00af00">173</span>, <span style="color: #00af00; text-decoration-color: #00af00">40</span>, <span style="color: #00af00; text-decoration-color: #00af00">16</span>)    │           <span style="color: #00af00; text-decoration-color: #00af00">304</span> │
├─────────────────────────────────┼────────────────────────┼───────────────┤
│ MaxPooling_1 (<span style="color: #0087ff; text-decoration-color: #0087ff">MaxPooling2D</span>)     │ (<span style="color: #00d7ff; text-decoration-color: #00d7ff">None</span>, <span style="color: #00af00; text-decoration-color: #00af00">86</span>, <span style="color: #00af00; text-decoration-color: #00af00">20</span>, <span style="color: #00af00; text-decoration-color: #00af00">16</span>)     │             <span style="color: #00af00; text-decoration-color: #00af00">0</span> │
├─────────────────────────────────┼────────────────────────┼───────────────┤
│ Conv_2 (<span style="color: #0087ff; text-decoration-color: #0087ff">Conv2D</span>)                 │ (<span style="color: #00d7ff; text-decoration-color: #00d7ff">None</span>, <span style="color: #00af00; text-decoration-color: #00af00">86</span>, <span style="color: #00af00; text-decoration-color: #00af00">20</span>, <span style="color: #00af00; text-decoration-color: #00af00">32</span>)     │         <span style="color: #00af00; text-decoration-color: #00af00">4,640</span> │
├─────────────────────────────────┼────────────────────────┼───────────────┤
│ MaxPooling_2 (<span style="color: #0087ff; text-decoration-color: #0087ff">MaxPooling2D</span>)     │ (<span style="color: #00d7ff; text-decoration-color: #00d7ff">None</span>, <span style="color: #00af00; text-decoration-color: #00af00">43</span>, <span style="color: #00af00; text-decoration-color: #00af00">10</span>, <span style="color: #00af00; text-decoration-color: #00af00">32</span>)     │             <span style="color: #00af00; text-decoration-color: #00af00">0</span> │
├─────────────────────────────────┼────────────────────────┼───────────────┤
│ Flatten (<span style="color: #0087ff; text-decoration-color: #0087ff">Flatten</span>)               │ (<span style="color: #00d7ff; text-decoration-color: #00d7ff">None</span>, <span style="color: #00af00; text-decoration-color: #00af00">13760</span>)          │             <span style="color: #00af00; text-decoration-color: #00af00">0</span> │
├─────────────────────────────────┼────────────────────────┼───────────────┤
│ Dropout (<span style="color: #0087ff; text-decoration-color: #0087ff">Dropout</span>)               │ (<span style="color: #00d7ff; text-decoration-color: #00d7ff">None</span>, <span style="color: #00af00; text-decoration-color: #00af00">13760</span>)          │             <span style="color: #00af00; text-decoration-color: #00af00">0</span> │
├─────────────────────────────────┼────────────────────────┼───────────────┤
│ Dense_1 (<span style="color: #0087ff; text-decoration-color: #0087ff">Dense</span>)                 │ (<span style="color: #00d7ff; text-decoration-color: #00d7ff">None</span>, <span style="color: #00af00; text-decoration-color: #00af00">64</span>)             │       <span style="color: #00af00; text-decoration-color: #00af00">880,704</span> │
├─────────────────────────────────┼────────────────────────┼───────────────┤
│ Dense_final (<span style="color: #0087ff; text-decoration-color: #0087ff">Dense</span>)             │ (<span style="color: #00d7ff; text-decoration-color: #00d7ff">None</span>, <span style="color: #00af00; text-decoration-color: #00af00">10</span>)             │           <span style="color: #00af00; text-decoration-color: #00af00">650</span> │
└─────────────────────────────────┴────────────────────────┴───────────────┘
</pre>

Использование нейронной сети сверточного типа обусловлено тем, что аудиоданные могут быть представлены в виде спектрограмм, которые по своей сути являются изображениями. Теоретически в качестве слоев можно использовать не только свёрточные, однако сама операция свёртки помогает одномоментно захватить несколько признаков. Комбинация с пулинговыми слоями помогают не учитывать смещения объектов на изображении, а также слои пулинга уменьшают размерность данных, сохраняя при этом важные признаки. Слой Dropout помогает избежать переобучения.

## Результаты

В качестве результата работы была создана, обучена и протестирована нейронная сеть. Сравнение результатов проводилось на платформе **kaggle**, поскольку архитектура нейросети требовательна к ресурсам GPU (на ресурсе kaggle был выбран ускоритель GPU T4 x2).  

Объемы входных данных будут зависеть от алгоритма их обработки. Однако сильного влияния на результаты обучения это не окажет. Опытным путем было установлено, что разница в точности предсказания моделью тестовых данных между двумя алгоритмами обработки составляет ~5%. Можно разве что отметить характер обучения - обучение на спектрограммах показаывает более плавное поведение, в отличие от обучения на мел-кепстральных коэффициентах.

Обучение на мел-кепстральных коэффициентах  
![График обучения на мел-кепстральных коэффициентах ](images/mel-train.png)

Обучение на спектральных коэффициентах  
![График обучения на спектральных коэффициентах ](images/spectr-train.png)